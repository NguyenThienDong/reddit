require("dotenv").config();
import express from "express";
import "reflect-metadata";
import { createConnection } from "typeorm";

const main = async () => {
  await createConnection({
    type: "postgres",
    username: process.env.DB_USERNAME_DEV,
    password: process.env.DB_PASSWORD_DEV,
    logging: true,
    synchronize: true,
  });

  const app = express();
  app.listen(4000, () => console.log('Server listening on port 4000'));
};

main().catch(error => console.log('Server error: ' + error.message));
